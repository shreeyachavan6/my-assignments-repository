var t1 = document.getElementById("t1"); //Returns the value of the element that has the Id specified ('t1' in this case)
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var t6 = document.getElementById("t6");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value); //typecasting is done since t1.value and t2.value are strings, the addition of which is not th eexpected result
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value); //typecasting is done since t1.value and t2.value are strings, the addition of which is not th eexpected result
    t4.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t5.value = c.toString();
}
function div() {
    var c = parseInt(t1.value) / parseInt(t2.value);
    t6.value = c.toString();
}
function clearall() {
    var c = "";
    t1.value = c.toString();
    t2.value = c.toString();
    t3.value = c.toString();
    t4.value = c.toString();
    t5.value = c.toString();
    t6.value = c.toString();
}
/*tsc --p tsconfig.json

tsc is a typescript compiler that tells how to compile the code*/ 
//# sourceMappingURL=app.js.map