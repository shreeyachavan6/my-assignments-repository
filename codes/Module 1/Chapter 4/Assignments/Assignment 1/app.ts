var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); //Returns the value of the element that has the Id specified ('t1' in this case)
var t2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var t3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");

var t4 : HTMLInputElement = <HTMLInputElement>document.getElementById("t4");

var t5 : HTMLInputElement = <HTMLInputElement>document.getElementById("t5");

var t6 : HTMLInputElement = <HTMLInputElement>document.getElementById("t6");

function add()
{
	var c:number = parseFloat(t1.value) + parseFloat(t2.value); //typecasting is done since t1.value and t2.value are strings, the addition of which is not th eexpected result
	t3.value = c.toString();
}
function sub()
{
	var c:number = parseFloat(t1.value) - parseFloat(t2.value); //typecasting is done since t1.value and t2.value are strings, the addition of which is not th eexpected result
	t4.value = c.toString();
}
function mul()
{
	var c:number = parseFloat(t1.value) * parseFloat(t2.value); 
	t5.value = c.toString();
}
function div()
{
	var c:number = parseInt(t1.value) / parseInt(t2.value); 
	t6.value = c.toString();
}
function clearall()
{	var c:string = "";
	t1.value = c.toString();
	t2.value = c.toString();
	t3.value = c.toString();
	t4.value = c.toString();
	t5.value = c.toString();
	t6.value = c.toString();
}
/*tsc --p tsconfig.json

tsc is a typescript compiler that tells how to compile the code*/